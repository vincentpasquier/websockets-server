/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.server.registry;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.restful.Plugin;
import ch.hesso.websocket.entities.PluginBean;
import ch.hesso.websocket.server.WebSocketManager;
import ch.hesso.websocket.server.bus.WebSocketEvents;
import ch.hesso.websocket.server.events.*;
import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.Session;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public enum WebSocketPlugins {
	REGISTRY;

	/**
	 * Server Hostname, TODO System.getProperty () or something to get
	 */
	public static final String WS_SERVER_HOSTNAME = "localhost";

	/**
	 * Server port, TODO look at System.getProperty ()
	 */
	public static final int WS_SERVER_PORT = 8025;

	/**
	 * Server path
	 */
	public static final String WS_SERVER_PATH = "/";

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( WebSocketPlugins.class );

	//
	private final Map<String, PluginBean> _plugins;

	//
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool ( 1 );

	//
	private int ports = 41784;

	//
	private WebSocketPlugins () {
		_plugins = new ConcurrentHashMap<> ();
		WebSocketEvents.BUS.register ( this );
		scheduler.scheduleAtFixedRate ( new PingWebSockets (), 0, 5, TimeUnit.SECONDS );
	}

	/**
	 * @return
	 */
	public synchronized Collection<PluginBean> plugins () {
		return _plugins.values ();
	}

	/**
	 * @param start
	 */
	@Subscribe
	public void onWSServerStart ( final WSServerStart start ) {
	}

	/**
	 * @param stop
	 */
	@Subscribe
	public void onWSServerStop ( final WSServerStop stop ) {
	}

	/**
	 * @param connexion
	 */
	@Subscribe
	public void onSessionStart ( final WSSessionStart connexion ) {
		String pluginId = connexion.pluginId ();
		boolean containsPluginId = _plugins.containsKey ( pluginId );
		if ( containsPluginId ) {
			PluginBean plugin = _plugins.get ( pluginId );
			plugin.addSession ( connexion.resource (), connexion.session () );
		}
	}

	/**
	 * @param connexion
	 */
	@Subscribe
	public void onSessionStop ( final WSSessionEnd connexion ) {
		String pluginId = connexion.pluginId ();
		boolean containsPluginId = _plugins.containsKey ( pluginId );
		if ( containsPluginId ) {
			PluginBean plugin = _plugins.remove ( pluginId );
			Map<String, Session> sessions = plugin.getSession ();
			sessions.remove ( connexion.resource () );
			plugin.setSession ( sessions );
			plugin.setMethod ( HTTPMethod.DELETE );
			Plugin pluginTO = new Plugin ();
			pluginTO.setId ( plugin.getId () );
			pluginTO.setName ( plugin.getName () );
			pluginTO.setWebSockets ( plugin.getWebSockets () );
			pluginTO.setWebSocket ( plugin.getWebSocket () );
			pluginTO.setRegistrations ( plugin.getRegistrations () );
			pluginTO.setMethod ( HTTPMethod.DELETE );
			Set<Session> registeredSessions = WebSocketManager.sessions (
					Plugin.class.getSimpleName ().toLowerCase (),
					HTTPMethod.DELETE );
			for ( Session session : registeredSessions ) {
				session.getAsyncRemote ().sendObject ( pluginTO );
			}
		}
	}

	/**
	 * @param pluginId
	 *
	 * @return
	 */
	public PluginBean plugin ( final String pluginId ) {
		return _plugins.get ( pluginId );
	}

	/**
	 * @param plugin
	 *
	 * @return
	 */
	public String generatePluginId ( final PluginBean plugin ) {
		String id = Integer.toHexString ( plugin.getName ().hashCode () );
		return id;
	}

	/**
	 * @param plugin
	 *
	 * @return
	 */
	public boolean register ( final PluginBean plugin ) {
		boolean register = false;
		for (; ; ) {
			/*
			Pre: The plugin must not contain any id
			1) Assign a key to the plugin
			2) Check whether plugin has already been registered
			3) Registers the plugin
			3.1) Assign a listen port for plugin communication
			3.2) Creates WebSockets for each registered resources
			4) Publish registered plugin
			 */
			String id = generatePluginId ( plugin );
			boolean containsKey = _plugins.containsKey ( id );
			if ( containsKey ) {
				break;
			}
			plugin.setId ( id );
			_plugins.put ( id, plugin );

			String localWebSocket = plugin.getWebSocket ();
			boolean hasLocalWebSocket = localWebSocket != null && localWebSocket.length () > 0;
			if ( hasLocalWebSocket ) {
				int localPort = nextAssignablePort ();
				String wsLocalPath = plugin.getWebSocket () + ":" + localPort;
				plugin.setWebSocket ( wsLocalPath );
			}
			Map<String, String> webSockets = webSocketsBasedOnRegistration ( plugin );
			plugin.setWebSockets ( webSockets );

			WebSocketEvents.BUS.publish ( new WSPluginRegistration ( plugin ) );
			register = true;
			LOG.info ( String.format ( "PluginBean registered [%s] with id [%s]", plugin.getName (), id ) );
			break;
		}
		return register;
	}

	/**
	 * @param bean
	 */
	public void unregister ( final PluginBean bean ) {
		if ( _plugins.containsKey ( bean.getId () ) ) {
			_plugins.remove ( bean.getId () );
		}
	}

	/**
	 * @return
	 */
	public synchronized int nextAssignablePort () {
		return ports++;
	}

	/**
	 * @param plugin
	 *
	 * @return
	 */
	public Map<String, String> webSocketsBasedOnRegistration ( final PluginBean plugin ) {
		Set<String> registrations = plugin.getRegistrations ().keySet ();
		Map<String, String> webSockets = new HashMap<> ();
		for ( String registration : registrations ) {
			// TODO Take into account external IP address to listen to

			String wsAddress = "ws://" + WS_SERVER_HOSTNAME + ":" + WS_SERVER_PORT + WS_SERVER_PATH;
			wsAddress += registration.toLowerCase () + "/" + plugin.getId ();
			webSockets.put ( registration, wsAddress );
		}
		return webSockets;
	}

	public boolean modify ( final String pluginId, final PluginBean delta ) {
		boolean success = _plugins.containsKey ( pluginId );
		if ( success ) {
			PluginBean bean = _plugins.get ( pluginId );
			bean.setName ( delta.getName () );
			bean.setDescription ( delta.getDescription () );
			// TODO Add more supported modify
			//bean.setRegistrations ( delta.getRegistrations () );
		}

		return success;
	}

	/**
	 *
	 */
	private final class PingWebSockets implements Runnable {

		private final ByteBuffer buffer = ByteBuffer.allocate ( 1 );

		public PingWebSockets () {
			buffer.put ( (byte) 0xFF );
		}

		@Override
		public void run () {
			// TODO If closed, remove
			for ( PluginBean plugin : plugins () ) {
				Collection<Session> sessions = plugin.getSession ().values ();
				for ( Session session : sessions ) {
					try {
						session.getBasicRemote ().sendPong ( buffer );
					} catch ( IOException e ) {
						e.printStackTrace ();
					}
				}
			}
		}
	}
}
