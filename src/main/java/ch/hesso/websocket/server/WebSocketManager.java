/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.server;


import ch.hesso.commons.HTTPMethod;
import ch.hesso.websocket.entities.PluginBean;
import ch.hesso.websocket.server.registry.WebSocketPlugins;

import javax.websocket.Session;
import java.util.*;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class WebSocketManager {

	/**
	 *
	 */
	public WebSocketManager () {
	}

	/**
	 * @param resource
	 * @param method
	 *
	 * @return
	 */
	public static Set<Session> sessions ( final String resource, final HTTPMethod method ) {
		Set<Session> sessions = new HashSet<> ();
		Collection<PluginBean> plugins = WebSocketPlugins.REGISTRY.plugins ();
		for ( PluginBean plugin : plugins ) {
			Map<String, Set<HTTPMethod>> registrations = plugin.getRegistrations ();
			/*
			1) Checks if plugin is registered to resource
			2) Checks if the plugin is registered to the method
			3) Checks if the plugin has a registered Session to this resource
			4) Finds the session, adds it to the resulting lists
			 */
			for (; ; ) {
				boolean contains = registrations.containsKey ( resource );
				if ( !contains ) {
					break;
				}

				boolean react = registrations.get ( resource ).contains ( method );
				if ( !react ) {
					break;
				}

				Map<String, Session> pluginSessions = plugin.getSession ();
				boolean containsSession = pluginSessions.containsKey ( resource );
				if ( !containsSession ) {
					break;
				}

				Session pluginSession = pluginSessions.get ( resource );
				sessions.add ( pluginSession );
				break;
			}
		}

		return sessions;
	}
}
