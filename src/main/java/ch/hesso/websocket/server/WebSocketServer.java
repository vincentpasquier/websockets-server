/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.server;

import ch.hesso.websocket.server.bus.WebSocketEvents;
import ch.hesso.websocket.server.endpoints.EndpointGenerator;
import ch.hesso.websocket.server.events.WSServerStart;
import org.glassfish.tyrus.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.DeploymentException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class WebSocketServer {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( WebSocketServer.class );

	//
	private final Server _server;

	//
	private final Builder _builder;

	//
	private WebSocketServer ( final Builder builder ) {
		_server = builder._server;
		_builder = builder;
	}

	/**
	 * @return
	 */
	public boolean start () {
		boolean success = true;
		try {
			_server.start ();
			WebSocketEvents.BUS.publish ( new WSServerStart ( _builder._hostname, _builder._port, _builder._path ) );
			LOG.info ( "WebSocket server starting." );
		} catch ( DeploymentException e ) {
			LOG.error ( "WebSocket server deployment failure. Check logs!", e );
			success = false;
		}
		return success;
	}

	/**
	 * @return
	 */
	public boolean stop () {
		_server.stop ();
		return true;
	}

	/**
	 *
	 */
	public static final class Builder {

		//
		private Server _server;

		//
		private String _hostname;

		//
		private int _port;

		//
		private String _path;

		//
		private Map<String, Object> _config;

		//
		private Builder ( final Set<Class<?>> possibleEndpoints ) {
			_hostname = "localhost";
			_port = 8025;
			_path = "/";
			_config = new HashMap<> ();
			EndpointGenerator.possibleEndpoints.addAll ( possibleEndpoints );
		}

		/**
		 * @param possibleEndpoints
		 *
		 * @return
		 */
		public static Builder create ( final Set<Class<?>> possibleEndpoints ) {
			return new Builder ( possibleEndpoints );
		}

		/**
		 * @param hostname
		 *
		 * @return
		 */
		public Builder hostname ( final String hostname ) {
			_hostname = hostname;
			return this;
		}

		/**
		 * @param port
		 *
		 * @return
		 */
		public Builder port ( final int port ) {
			_port = port;
			return this;
		}

		/**
		 * @param path
		 *
		 * @return
		 */
		public Builder path ( final String path ) {
			_path = path;
			return this;
		}

		/**
		 * @param config
		 *
		 * @return
		 */
		public Builder configuration ( final Map<String, Object> config ) {
			_config = config;
			return this;
		}

		/**
		 * @return
		 */
		public WebSocketServer build () {
			_server = new Server ( _hostname, _port, _path, _config, EndpointGenerator.class );
			LOG.info ( "WebSocket server built" );
			return new WebSocketServer ( this );
		}
	}
}
