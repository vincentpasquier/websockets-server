/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.server.endpoints;

import ch.hesso.websocket.server.bus.WebSocketEvents;
import ch.hesso.websocket.server.events.WSSessionEnd;
import ch.hesso.websocket.server.events.WSSessionStart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import java.io.IOException;
import java.util.Map;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ServerEndpoint extends Endpoint {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( ServerEndpoint.class );

	/**
	 * @param session
	 * @param config
	 */
	@Override
	public void onOpen ( final Session session, final EndpointConfig config ) {
		String[] tokens = session.getRequestURI ().getPath ().split ( "/" );
		Map<String, String> params = session.getPathParameters ();
		if ( params.containsKey ( "id" ) && tokens.length >= 2 ) {
			String type = tokens[ 1 ];
			String pluginId = params.get ( "id" );
			WebSocketEvents.BUS.publish ( new WSSessionStart ( session, pluginId, type ) );
			LOG.info ( String.format ( "Successfully subscribed plugin to %s with ID %s !", type, pluginId ) );
		} else {
			try {
				LOG.info ( "A plugin tried to connect with an invalid Identifier." );
				session.close ();
			} catch ( IOException e ) {
			}
		}
	}

	/**
	 * @param session
	 * @param closeReason
	 */
	@Override
	public void onClose ( final Session session, final CloseReason closeReason ) {
		String[] tokens = session.getRequestURI ().getPath ().split ( "/" );
		Map<String, String> params = session.getPathParameters ();
		if ( params.containsKey ( "id" ) && tokens.length >= 2 ) {
			String type = tokens[ 1 ];
			String pluginId = params.get ( "id" );
			WebSocketEvents.BUS.publish ( new WSSessionEnd ( session, pluginId, type ) );
			LOG.info ( String.format ( "Successfully unregistered plugin to %s with ID %s !", type, pluginId ) );
		} else {
			try {
				LOG.info ( "A plugin tried to connect with an invalid Identifier." );
				session.close ();
			} catch ( IOException e ) {
			}
		}
	}
}
