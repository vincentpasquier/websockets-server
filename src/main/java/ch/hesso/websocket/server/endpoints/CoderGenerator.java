/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.server.endpoints;

import ch.hesso.websocket.entities.JSONCoder;
import javassist.*;

import javax.websocket.Decoder;
import javax.websocket.Encoder;

/**
 * @param <C>
 *
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class CoderGenerator<C extends Encoder.TextStream & Decoder.TextStream> {

	/**
	 *
	 */
	public static final String GENERATED_PREFIX = "WS_GEN_";

	/**
	 *
	 */
	public static final String PREFIX_CODER = GENERATED_PREFIX + "CODER_";

	//
	private final ClassPool _cPool;

	//
	public CoderGenerator ( final Class<C> coder ) {
		_cPool = ClassPool.getDefault ();
		_cPool.insertClassPath ( new ClassClassPath ( JSONCoder.class ) );
	}

	/**
	 * Creates a coder which supports Encoder and Decoder TextStream.
	 * <p/>
	 * Generic names are of the type are similar to (discard - -) Written with / and not dot (.), replaced at the end
	 * L-some/class-<L-some/class-;>;
	 *
	 * @param possibleEndpoint
	 *
	 * @return
	 *
	 * @throws CannotCompileException
	 */
	@SuppressWarnings ( "unchecked" )
	public Class<C> makeCoder ( final Class<?> possibleEndpoint ) throws Exception {
		try {
			CtClass $ctCoder = _cPool.get ( JSONCoder.class.getCanonicalName () );
			CtClass ctCoder;
			try {
				ctCoder = _cPool.get ( PREFIX_CODER + possibleEndpoint.getSimpleName () );
			} catch ( NotFoundException e ) {
				ctCoder = _cPool.makeClass ( PREFIX_CODER + possibleEndpoint.getSimpleName (), $ctCoder );
			}
			if ( ctCoder.isFrozen () ) {
				Class<?> classCoder = _cPool.getClassLoader ().loadClass ( PREFIX_CODER + possibleEndpoint.getSimpleName () );
				// W: This class has been generated, hopefully, by this method which only generates C classes
				return (Class<C>) classCoder;
			}
			StringBuilder genericSignature = new StringBuilder ( "L" );
			genericSignature.append ( $ctCoder.getName () );
			genericSignature.append ( "<L" );
			genericSignature.append ( possibleEndpoint.getCanonicalName () );
			genericSignature.append ( ";>;" );
			String signature = genericSignature.toString ().replace ( ".", "/" );
			ctCoder.setGenericSignature ( signature );
			return ctCoder.toClass ();
		} catch ( CannotCompileException | NotFoundException e ) {
			e.printStackTrace ();
			throw new Exception ( e );
		}
	}


}
