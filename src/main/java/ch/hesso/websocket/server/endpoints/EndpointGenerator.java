/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.server.endpoints;

import ch.hesso.websocket.entities.JSONCoder;
import ch.hesso.websocket.entities.PluginBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.Decoder;
import javax.websocket.Encoder;
import javax.websocket.Endpoint;
import javax.websocket.server.ServerApplicationConfig;
import javax.websocket.server.ServerEndpointConfig;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class EndpointGenerator implements ServerApplicationConfig {

	//
	public final static Set<Class<?>> possibleEndpoints = new HashSet<> ();

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( EndpointGenerator.class );

	//
	public static boolean registration = false;

	//
	private final Set<ServerEndpointConfig> _endpoints = new HashSet<> ();

	//
	private final CoderGenerator<JSONCoder> _coderGenerator;

	/**
	 *
	 */
	public EndpointGenerator () {
		_coderGenerator = new CoderGenerator<> ( JSONCoder.class );
		for ( Class<?> possibleEndpoint : possibleEndpoints ) {
			_endpoints.add ( buildEndpoint ( ServerEndpoint.class, possibleEndpoint, possibleEndpoint.getSimpleName ().toLowerCase (), "{id}" ) );
		}
	}

	/**
	 * @param endpoint
	 * @param $class
	 * @param path
	 * @param parameters
	 *
	 * @return
	 */
	@SuppressWarnings ( "unchecked" )
	public ServerEndpointConfig buildEndpoint ( final Class<?> endpoint,
																							final Class<?> $class,
																							final String path,
																							final String parameters ) {
		String endpointPath = "/" + path + ( parameters == null ? "" : ( "/" + parameters ) );
		ServerEndpointConfig.Builder builder = ServerEndpointConfig.Builder.create ( endpoint, endpointPath );
		try {
			Class<? extends Encoder.TextStream> coder = _coderGenerator.makeCoder ( $class );
			// Unchecked: makeCoder always returns a class that extends both Encoder and Decoder TextStream
			Class<? extends Decoder.TextStream> decoder = (Class<? extends Decoder.TextStream>) coder;
			builder.encoders ( Arrays.<Class<? extends Encoder>>asList ( coder ) );
			builder.decoders ( Arrays.<Class<? extends Decoder>>asList ( decoder ) );
			LOG.info ( String.format ( "Generated endpoint with [%s] endpoint, [%s] class, [%s] path", endpoint.getSimpleName (), $class.getSimpleName (), endpointPath ) );
		} catch ( Exception e ) {
			LOG.error ( "Error while creating coder", e );
		}
		return builder.build ();
	}

	@Override
	public Set<ServerEndpointConfig> getEndpointConfigs ( final Set<Class<? extends Endpoint>> endpointClasses ) {
		if ( registration ) {
			_endpoints.add ( buildEndpoint ( RegistrationEndpoint.class, PluginBean.class, "", null ) );
		}
		return _endpoints;
	}

	@Override
	public Set<Class<?>> getAnnotatedEndpointClasses ( final Set<Class<?>> scanned ) {
		return scanned;
	}
}
