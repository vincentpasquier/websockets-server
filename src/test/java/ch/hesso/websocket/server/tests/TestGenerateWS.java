/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.server.tests;

import ch.hesso.predict.restful.APIEntity;
import ch.hesso.reflect.TopLevelClassFinder;
import ch.hesso.websocket.server.WebSocketServer;
import scala.unchecked;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestGenerateWS {

	/**
	 * Server Hostname, TODO look at System.getProperty ()
	 */
	public static final String WS_SERVER_HOSTNAME = "localhost";

	/**
	 * Server port, TODO look at System.getProperty ()
	 */
	public static final int WS_SERVER_PORT = 8025;

	/**
	 * Server path
	 */
	public static final String WS_SERVER_PATH = "/";

	public static void main ( final String[] args ) throws IOException {
		TopLevelClassFinder<APIEntity> finder = new TopLevelClassFinder<> ();
		Set<Class<? extends APIEntity>> tmpEndpoints = finder.findInstantiableSubClasses ( APIEntity.class, "ch.hesso.predict" );
		Set<Class<?>> endpoints = new HashSet<> ();
		for ( Class<?> tmpEndpoint : tmpEndpoints ) {
			endpoints.add ( tmpEndpoint );
		}
		WebSocketServer.Builder builder = WebSocketServer.Builder.create ( endpoints );
		builder.hostname ( WS_SERVER_HOSTNAME );
		builder.port ( WS_SERVER_PORT );
		builder.path ( WS_SERVER_PATH );
		WebSocketServer server = builder.build ();
		server.start ();
		System.in.read ();
	}
}
