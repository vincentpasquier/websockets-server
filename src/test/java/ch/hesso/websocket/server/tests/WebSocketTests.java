/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.server.tests;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.websocket.entities.PluginBean;
import org.glassfish.tyrus.client.ClientManager;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class WebSocketTests {

	public static void main ( final String[] args ) throws IOException, DeploymentException, EncodeException {


		ClientManager client = ClientManager.createClient ();
		PluginEndpoint endpoint = new PluginEndpoint ();
		Session session = client.connectToServer ( endpoint,
				ClientEndpointConfig.Builder.create ()
						.encoders ( Arrays.<Class<? extends Encoder>>asList ( PluginBean.PluginCoder.class ) )
						.decoders ( Arrays.<Class<? extends Decoder>>asList ( PluginBean.PluginCoder.class ) )
						.build (),
				URI.create ( "ws://localhost:8025/" ) );
		PluginBean.Builder builder = PluginBean.Builder.newBuilder ( "heyman" );
		builder.registration ( "cfp", HTTPMethod.POST );
		builder.registration ( "plugin", HTTPMethod.POST, HTTPMethod.DELETE );
		//builder.types ( PluginBean.Type.EXTRACTOR, PluginBean.Type.UI );
		PluginBean plugin = builder.build ();
		session.getBasicRemote ().sendObject ( plugin );

		System.in.read ();
	}
}
