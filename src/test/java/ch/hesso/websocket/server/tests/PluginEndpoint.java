/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.server.tests;


import ch.hesso.websocket.entities.PluginBean;

import javax.websocket.*;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PluginEndpoint extends Endpoint {

	@Override
	public void onOpen ( final Session session, final EndpointConfig config ) {
		session.addMessageHandler ( new MessageHandler.Whole<PluginBean> () {
			@Override
			public void onMessage ( final PluginBean message ) {
				System.out.println ( message.toString () );
			}
		} );
	}

	@Override
	public void onClose ( final Session session, final CloseReason reason ) {
		System.out.println ( "CLOSED!" );
	}
}
